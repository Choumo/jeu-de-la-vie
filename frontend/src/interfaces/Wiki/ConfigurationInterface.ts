export interface WikiConfigurationInterface {
    name: string;
    type: string;
    boundingBox: string;
    nbCell: number;
    period?: number;
    speed?: number;
}
