import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import WikiView from '@/views/WikiView.vue'
import CreateConfigurationView from '@/views/Dashboard/CreateConfigurationView.vue'
import DashboardView from '@/views/Dashboard/DashboardView.vue'
import SimulatorView from '@/views/SimulatorView.vue'
import UpdateConfiguration from '@/views/Dashboard/UpdateConfiguration.vue'
import RegisterView from '@/views/RegisterView.vue'
import LoginView from '@/views/LoginView.vue'
import RulesView from '@/views/RulesView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      children: [
        {
          path: '',
          component: DashboardView
        },
        {
          path: 'nouvelle-configuration',
          component: CreateConfigurationView
        },
        {
          path: 'edit-configuration/:id',
          component: UpdateConfiguration
        }
      ]
    },
    {
      path: '/wiki',
      name: 'wiki',
      component: WikiView
    },
    {
      path: '/simulator',
      name: 'simulator',
      component: SimulatorView
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterView
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView
    },
    {
      path: '/rules',
      name: 'rules',
      component: RulesView
    }
  ]
})

export default router
