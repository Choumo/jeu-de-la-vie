const express = require('express');
const router = express.Router();
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const jwt = require('jsonwebtoken');

const authenticateToken = (req, res, next) => {
  const token = req.header('AUTH_TOKEN');
  if (!token) return res.status(401).send('Access denied');

  try {
    const verified = jwt.verify(token, process.env.SECRET_KEY);

    if(!verified) return res.status(401).send('Access denied');

    next();
  } catch (err) {
    res.status(400).send('Invalid token');
  }
};

module.exports = authenticateToken;
