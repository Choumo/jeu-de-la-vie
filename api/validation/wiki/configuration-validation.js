const Joi = require('joi');

const schemaPost = Joi.object({
  name: Joi.string().required(),
  type: Joi.string().valid('Nature morte', 'Oscillateurs', 'Vaisseaux', 'Guns').required(),
  boundingBox: Joi.string().pattern(/^\d+x\d+$/).required(),
  nbCell: Joi.number().min(1).required(),
  period: Joi.number().min(1).optional()
}).custom((value, helpers) => {
  const [width, height] = value.boundingBox.split('x').map(Number);
  const maxCells = width * height;
  if (value.nbCell > maxCells) {
    return helpers.error('any.invalid', { message: 'nbCell cannot be greater than the bounding box area' });
  }
  return value;
});

const schemaPut = schemaPost;

module.exports = {
  schemaPost,
  schemaPut
};
