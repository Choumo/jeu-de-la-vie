-- CreateTable
CREATE TABLE "Configuration" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "boundingBox" VARCHAR(255) NOT NULL,
    "nbCell" INTEGER NOT NULL,
    "period" INTEGER NOT NULL,
    "speed" REAL NOT NULL,

    CONSTRAINT "Configuration_pkey" PRIMARY KEY ("id")
);
