/*
  Warnings:

  - Added the required column `type` to the `Configuration` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Configuration" ADD COLUMN     "type" VARCHAR(255) NOT NULL,
ALTER COLUMN "period" DROP NOT NULL,
ALTER COLUMN "speed" DROP NOT NULL;
