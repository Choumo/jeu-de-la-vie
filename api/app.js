const express = require('express');
const app = express();
const cors = require('cors')

app.use(cors())

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routers
const wikiRouter = require('./routes/wiki.js');
app.use('/wiki', wikiRouter);

const userRouter = require('./routes/user.js');
app.use('/user', userRouter);


// Server config
let hostname = '127.0.0.1';
const port = process.env.PORT || 3000;
if(process.env.IS_DOCKER === "TRUE") {
    hostname = '0.0.0.0';
}

app.listen(port, hostname, () => {
    console.log(`Serveur démarré sur http://${hostname}:${port}`);
});