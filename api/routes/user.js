const express = require('express');
const router = express.Router();
const { PrismaClient } = require('@prisma/client');
const schema = require('../validation/wiki/configuration-validation.js');
const { userSchema } = require('../validation/user/user-validation')
const prisma = new PrismaClient();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

router.post('/register', async (req, res) => {
  try {
    const { error } = userSchema.validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const { username, password } = req.body;
    const hashedPassword = await bcrypt.hash(password, 10);

    const user = await prisma.User.create({
      data: {
        username: username,
        password: hashedPassword,
      },
    });

    res.status(201).send('User registered successfully');
  } catch (err) {
    res.status(500).send('Internal server error');
  }
});

router.post('/login', async (req, res) => {
  try {
    const { error } = userSchema.validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const { username, password } = req.body;
    const user = await prisma.User.findUnique({
      where: {
        username: username
      },
    });

    if (!user) return res.status(400).send('Invalid username or password');

    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) return res.status(400).send('Invalid username or password');

    const token = jwt.sign({ userId: user.id }, process.env.SECRET_KEY, { expiresIn: '1h' });
    res.json({ token });
  } catch (err) {
    res.status(500).send('Internal server error');
  }
});

module.exports = router;
