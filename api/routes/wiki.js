const express = require('express');
const router = express.Router();
const { PrismaClient } = require('@prisma/client');
const schema = require('../validation/wiki/configuration-validation.js');
const authenticateToken = require('../middleware/checkAuth')
const prisma = new PrismaClient();

router.get('/', async function (req, res) {
    try {
        const configurations = await prisma.Configuration.findMany({});
        return res.status(200).send({
            status: 200,
            data: configurations,
        });
    } catch (err) {
        return res.status(422).send({
            status: 422,
            message: err.message,
        });
    }
});

router.get('/:id', async function(req, res) {
    try {
        const configurationId = parseInt(req.params.id);
        const configuration = await prisma.Configuration.findUnique({
            where: {
                id: configurationId,
            },
        });

        if (!configuration) {
            return res.status(404).send({
                status: 404,
                message: 'Configuration not found',
            });
        }

        return res.status(200).send({
            status: 200,
            data: configuration,
        });
    } catch (err) {
        return res.status(422).send({
            status: 422,
            message: err.message,
        });
    }
});

router.post('/', authenticateToken, async function (req, res) {
    try {
        const value = await schema.schemaPost.validateAsync(req.body);

        const configurationData = {
            name: req.body.name,
            type: req.body.type,
            boundingBox: req.body.boundingBox,
            nbCell: req.body.nbCell,
            period: req.body.period || null,
        };

        // Calculate speed if type is Vaisseaux
        if (configurationData.type === 'Vaisseaux' && configurationData.period) {
            configurationData.speed = configurationData.nbCell / configurationData.period;
        }

        const configuration = await prisma.Configuration.create({
            data: configurationData,
        });

        return res.status(201).send({
            status: 201,
            message: 'Configuration successfully saved',
        });
    } catch (err) {
        return res.status(422).send({
            status: 422,
            message: err.message,
        });
    }
});

router.put('/:id(\\d+)', authenticateToken, async function (req, res) {
    try {
        const value = await schema.schemaPut.validateAsync(req.body);

        const configurationData = {
            name: req.body.name,
            type: req.body.type,
            boundingBox: req.body.boundingBox,
            nbCell: req.body.nbCell,
            period: req.body.period || null,
        };

        // Calculate speed if type is Vaisseaux
        if (configurationData.type === 'Vaisseaux' && configurationData.period) {
            configurationData.speed = configurationData.nbCell / configurationData.period;
        }

        const configuration = await prisma.Configuration.update({
            where: {
                id: parseInt(req.params.id),
            },
            data: configurationData,
        });

        return res.status(200).send({
            status: 200,
            message: 'Configuration successfully updated',
        });
    } catch (err) {
        return res.status(422).send({
            status: 422,
            message: err.message,
        });
    }
});

router.delete('/:id(\\d+)', authenticateToken, async function (req, res) {
    try {
        const configuration = await prisma.Configuration.delete({
            where: {
                id: parseInt(req.params.id),
            },
        });

        return res.status(200).send({
            status: 200,
            message: 'Configuration successfully deleted',
        });
    } catch (err) {
        return res.status(422).send({
            status: 422,
            message: err.message,
        });
    }
});

module.exports = router;
